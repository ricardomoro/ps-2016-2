<ul id="legenda-regioes">
    <span>Regi&otilde;es:</span>
    <li class="item-LITORAL"><span class="glyphicon glyphicon-stop"></span>&nbsp;Litoral Norte</li>
    <li class="item-SUL"><span class="glyphicon glyphicon-stop"></span>&nbsp;Litoral Sul</li>
    <li class="item-METROPOLITANA"><span class="glyphicon glyphicon-stop"></span>&nbsp;Metropolitana</li>
    <li class="item-NORTE"><span class="glyphicon glyphicon-stop"></span>&nbsp;Norte</li>
    <li class="item-SERRA"><span class="glyphicon glyphicon-stop"></span>&nbsp;Serra</li>
    <li class="item-VALE-CAI"><span class="glyphicon glyphicon-stop"></span>&nbsp;Vale do Ca&iacute;</li>
    <li class="item-ENCOSTA-SERRA"><span class="glyphicon glyphicon-stop"></span>&nbsp;Vale do Paranhana</li>
</ul>

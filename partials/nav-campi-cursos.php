<div class="row">
    <div class="col-xs-12">
        <ul class="list-campi" role="tablist">
            <li class="active"><a href="#tab-todos" class="btn btn-todos" role="button" data-toggle="tab">Todos</a></li>
            <?php $terms = get_terms('campus'); ?>
            <?php foreach ($terms as $key => $campus) : ?>
                <li><a href="#tab-<?php echo $campus->slug; ?>" class="btn btn-<?php echo $campus->slug; ?>" role="button" data-toggle="tab"><?php echo $campus->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

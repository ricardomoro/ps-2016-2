<?php if (have_posts()) : ?>
    <div class="list-group">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php $parent = wp_get_post_parent_id(get_the_ID()); ?>
            <a href="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), 'upload_file', true)); ?>" rel="bookmark" class="list-group-item<?php echo (empty($parent) ? ' active' : ''); ?>" target="_blank" title="<?php the_title(); ?> (abre em uma nova p&aacute;gina)">
                <h4 class="list-group-item-heading"><?php if (!empty($parent)) : ?><span class="glyphicon glyphicon-minus"></span>&nbsp;<?php endif; ?><?php the_title(); ?><span class="sr-only">&nbsp;(abre uma nova p&aacute;gina)</span><span class="glyphicon glyphicon-new-window pull-right"></span></h4>
                <p class="list-group-item-text"><small><span class="glyphicon glyphicon-calendar"></span>&nbsp;<?php if (get_the_modified_time() != get_the_time()) : ?>atualizado em <?php the_modified_time('d'); ?> de <?php the_modified_time('F'); ?> de <?php the_modified_time('Y'); ?>&nbsp; | &nbsp;<?php endif; ?>publicado em <?php the_time('d'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?></small></p>
            </a>
        <?php endwhile; ?>
    </div>
<?php else : ?>
    <div class="alert alert-warning" role="alert">
        <p><strong>Aguarde!</strong> Em breve os editais ser&atilde;o publicados.</p>
    </div>
<?php endif; ?>

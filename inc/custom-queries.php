<?php
function ps_custom_queries( $query ) {
    if ($query->is_main_query()) {
        if ($query->is_home()) {
            $faq = get_category_by_slug('faq');
            $query->query_vars['category__not_in'] = array($faq->term_id);
        }

        if ($query->is_category('faq')) {
            $query->query_vars['posts_per_page'] = 9999;
        }

        if ($query->is_post_type_archive('edital')) {
            // $query->query_vars['orderby'] = 'menu_order title';
            // $query->query_vars['order'] = 'ASC';
            $query->query_vars['posts_per_page'] = 9999;
        }
    }
}

add_action( 'pre_get_posts', 'ps_custom_queries' );

<?php
/**
 * Adds Mapa_Widget widget.
 */
class Mapa_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mapa_widget', // Base ID
			__( 'Mapa PS', 'ps20162' ), // Name
			array( 'description' => __( 'Mapa do RS com as regiões de abrangência do IFRS', 'ps20162' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
        wp_enqueue_style('css-ammap3', get_stylesheet_directory_uri().'/vendor/ammap3/ammap/ammap.css', array(), false, 'all');
        wp_enqueue_script( 'ammap3', get_stylesheet_directory_uri().'/vendor/ammap3/ammap/ammap.js', array(), false, false );
		if (WP_DEBUG) {
			wp_enqueue_script( 'ammap3-config', get_stylesheet_directory_uri().'/src/ammap3-config.js', array(), false, true );
		} else {
			wp_enqueue_script( 'ammap3-config', get_stylesheet_directory_uri().'/js/ammap3-config.min.js', array(), false, true );
		}

        $wnm_custom = array( 'stylesheet_directory_uri' => get_stylesheet_directory_uri() );
        wp_localize_script( 'ammap3-config', 'wp', $wnm_custom );

		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		?>
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<div class="center-block" id="mapa-ps"></div>
				</div>
				<div class="col-xs-12 col-md-4">
					<span class="center-block glyphicon glyphicon-arrow-down" id="mapa-seta" aria-hidden="true"></span>

					<div class="panel-group" id="mapa-accordion" role="tablist">
						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-LITORAL">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-LITORAL" aria-expanded="true" aria-controls="collapse-LITORAL" id="select-LITORAL">
										Litoral Norte
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-LITORAL" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-LITORAL">
								<div class="panel-body">
									<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-osorio" class="btn btn-primary btn-xs">Campus Os&oacute;rio</a>
								</div>
							</div>
						</div>

						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-SUL">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-SUL" aria-expanded="true" aria-controls="collapse-SUL" id="select-SUL">
										Litoral Sul
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-SUL" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-SUL">
								<div class="panel-body">
									<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-rio-grande" class="btn btn-primary btn-xs">Campus Rio Grande</a>
								</div>
							</div>
						</div>

						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-METROPOLITANA">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-METROPOLITANA" aria-expanded="true" aria-controls="collapse-METROPOLITANA" id="select-METROPOLITANA">
										Metropolitana
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-METROPOLITANA" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-METROPOLITANA">
								<div class="panel-body">
									<div class="btn-group-vertical" role="group" aria-label="Campi da Regi&atilde;o Metropolitana">
										<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-alvorada" class="btn btn-primary btn-xs">Campus Alvorada</a>
										<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-porto-alegre" class="btn btn-primary btn-xs">Campus Porto Alegre</a>
										<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-restinga" class="btn btn-primary btn-xs">Campus Restinga</a>
										<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-viamao" class="btn btn-primary btn-xs">Campus Viam&atilde;o</a>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-NORTE">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-NORTE" aria-expanded="true" aria-controls="collapse-NORTE" id="select-NORTE">
										Norte
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-NORTE" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-NORTE">
								<div class="panel-body">
									<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-erechim" class="btn btn-primary btn-xs">Campus Erechim</a>
								</div>
							</div>
						</div>

						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-SERRA">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-SERRA" aria-expanded="true" aria-controls="collapse-SERRA" id="select-SERRA">
										Serra
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-SERRA" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-SERRA">
								<div class="panel-body">
									<div class="btn-group-vertical" role="group" aria-label="Campi da Regi&atilde;o da Serra">
										<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-bento-goncalves" class="btn btn-primary btn-xs">Campus Bento Gon&ccedil;alves</a>
										<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-caxias-do-sul" class="btn btn-primary btn-xs">Campus Caxias do Sul</a>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-VALE-CAI">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-VALE-CAI" aria-expanded="true" aria-controls="collapse-VALE-CAI"  id="select-VALE-CAI">
										Vale do Ca&iacute;
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-VALE-CAI" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-VALE-CAI">
								<div class="panel-body">
									<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-feliz" class="btn btn-primary btn-xs">Campus Feliz</a>
								</div>
							</div>
						</div>

						<div class="panel panel-mapa-ps">
							<div class="panel-heading" role="tab" id="heading-ENCOSTA-SERRA">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#mapa-accordion" href="#collapse-ENCOSTA-SERRA" aria-expanded="true" aria-controls="collapse-ENCOSTA-SERRA" id="select-ENCOSTA-SERRA">
										V. do Paranhana
										<span class="glyphicon glyphicon-stop pull-right"></span>
									</a>
								</h4>
							</div>
							<div id="collapse-ENCOSTA-SERRA" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ENCOSTA-SERRA">
								<div class="panel-body">
									<a href="<?php echo get_post_type_archive_link( 'curso' ); ?>#tab-rolante" class="btn btn-primary btn-xs">Campus Rolante</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        <?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'text_domain' );
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget

// register Foo_Widget widget
function register_mapa_widget() {
    register_widget( 'Mapa_Widget' );
}
add_action( 'widgets_init', 'register_mapa_widget' );

<?php
function widgets_init() {
	register_sidebar(array(
		'name' => 'Banner Conteúdo',
		'id' => 'banner',
		'description' => 'Banner acima do conteúdo.',
		'before_widget' => '<!--widget--><div id="%1$s" class="widget banner %2$s">',
		'after_widget'  => '</div><!--//widget-->',
		'before_title'  => '<span class="sr-only">',
		'after_title'   => '</span>',
	));
	register_sidebar(array(
		'name' => 'Home 1',
		'id' => 'home_1',
		'description' => 'Widget na página inicial.',
		'before_widget' => '<!--widget--><div id="%1$s" class="widget-home %2$s">',
		'after_widget'  => '</div><!--//widget-->',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name' => 'Home 2',
		'id' => 'home_2',
		'description' => 'Widget na página inicial.',
		'before_widget' => '<!--widget--><div id="%1$s" class="widget-home %2$s">',
		'after_widget'  => '</div><!--//widget-->',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	));
}
add_action( 'widgets_init', 'widgets_init' );

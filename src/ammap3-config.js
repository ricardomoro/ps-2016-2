var map = AmCharts.makeChart( "mapa-ps", {
    /**
    * this tells amCharts it's a map
    */
    "type": "map",

    /**
    * Allows changing language easily. Note, you should include language js file from
    * amcharts/lang or ammap/lang folder and then use variable name used in this file, like
    * chart.language = "de"; Note, for maps this works differently - you use language only for
    * country names, as there are no other strings in the maps application.
    */
    "language": "pt",

    /**
    * Specifies if the map is draggable.
    */
    "dragMap": false,
    "preventDragOut": true,

    "allowClickOnSelectedObject": false,

    /**
    * let's say we want a small map to be displayed, so let's create it
    */
    "smallMap": {
        "enabled": false,
    },

    "zoomOnDoubleClick": false,

    "zoomControl": {
		"zoomControlEnabled": false,
        "homeButtonEnabled": true,
	},

    /**
    * create data provider object
    * map property is usually the same as the name of the map file.
    * getAreasFromMap indicates that amMap should read all the areas available
    * in the map data and treat them as they are included in your data provider.
    * in case you don't set it to true, all the areas except listed in data
    * provider will be treated as unlisted.
    */
    "dataProvider": {
        "mapURL": wp.stylesheet_directory_uri + "/img/mapa-ps.svg",
        "areas": [
            {
                "id": "RS",
                "color": "#71C59B",
                "selectedColor": "#71C59B",
                "outlineColor": "#71C59B",
                "selectable": false,
                "mouseEnabled": false,
            },
            {
                "id": "SUL",
                "color": "#007769",
                "selectedColor": "#007769",
                "outlineColor": "#007769",
                "mouseEnabled": false,
            },
            {
                "id": "NORTE",
                "color": "#7E1648",
                "selectedColor": "#7E1648",
                "outlineColor": "#7E1648",
                "mouseEnabled": false,
            },
            {
                "id": "LITORAL",
                "color": "#E80F6D",
                "selectedColor": "#E80F6D",
                "outlineColor": "#E80F6D",
                "mouseEnabled": false,
            },
            {
                "id": "METROPOLITANA",
                "color": "#E8DA36",
                "selectedColor": "#E8DA36",
                "outlineColor": "#E8DA36",
                "mouseEnabled": false,
            },
            {
                "id": "ENCOSTA-SERRA",
                "color": "#E97C24",
                "selectedColor": "#E97C24",
                "outlineColor": "#E97C24",
                "mouseEnabled": false,
            },
            {
                "id": "SERRA",
                "color": "#642C8D",
                "selectedColor": "#642C8D",
                "outlineColor": "#642C8D",
                "mouseEnabled": false,
            },
            {
                "id": "VALE-CAI",
                "color": "#547F3A",
                "selectedColor": "#547F3A",
                "outlineColor": "#547F3A",
                "mouseEnabled": false,
            },
        ],
        "images": [
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Alvorada",
                "labelColor": "#000000",
                "labelRollOverColor": "#000000",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 32.5,
                "longitude": 118,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Bento Gonçalves",
                "labelColor": "#FFFFFF",
                "labelRollOverColor": "#FFFFFF",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 73,
                "longitude": 92,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Caxias do Sul",
                "labelColor": "#FFFFFF",
                "labelRollOverColor": "#FFFFFF",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 71,
                "longitude": 110,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Erechim",
                "labelColor": "#FFFFFF",
                "labelRollOverColor": "#FFFFFF",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 87.7,
                "longitude": 60,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Feliz",
                "labelColor": "#FFFFFF",
                "labelRollOverColor": "#FFFFFF",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 60,
                "longitude": 105,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Osório",
                "labelColor": "#000000",
                "labelRollOverColor": "#000000",
                "labelShiftX": -3,
                "labelShiftY": -3,
                "latitude": 38,
                "longitude": 155,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Porto Alegre",
                "labelColor": "#000000",
                "labelRollOverColor": "#000000",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 24,
                "longitude": 112,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Restinga",
                "labelColor": "#000000",
                "labelRollOverColor": "#000000",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 20,
                "longitude": 112,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Rio Grande",
                "labelFontSize": 10,
                "labelColor": "#000000",
                "labelRollOverColor": "#000000",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": -79.3,
                "longitude": 63,
                "passZoomValuesToTarget": true,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Rolante",
                "labelColor": "#FFFFFF",
                "labelRollOverColor": "#FFFFFF",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 54,
                "longitude": 139,
            },
            {
                "groupId": "campi",
                "type": "circle",
                "color": "#FFFFFF",
                "label": "Viamão",
                "labelColor": "#000000",
                "labelRollOverColor": "#000000",
                "labelShiftX": -7,
                "labelShiftY": -3,
                "latitude": 28,
                "longitude": 120,
            },
        ],
    },

    /**
    * create areas settings
    */
    "areasSettings": {
        "autoZoom": true,
        "outlineThickness": 2,
        "selectedOutlineColor": "#FAFAFA",
        "selectedOutlineThickness": 2,
    },

    "listeners": [
        {
            "event": "homeButtonClicked",
            "method": function(e) {
                $('#mapa-accordion .collapse').each(function(index, element) {
                    $(element).collapse('hide');
                });
            },
        },
    ],
} );

// create a zoom listener which will check current zoom level and will toggle
// corresponding image groups accordingly
map.addListener("rendered", function (e) {
    revealMapImages(e);
    map.addListener("zoomCompleted", revealMapImages);
});

function revealMapImages (event) {
    var zoomLevel = map.zoomLevel();
    if ( zoomLevel <= 1 ) {
        map.hideGroup("campi");
    } else {
        map.showGroup("campi");
    }
}

$('#mapa-accordion .collapse').on('hide.bs.collapse', function() {
    map.selectObject();
});

$('#collapse-SUL').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('SUL'));
});
$('#collapse-NORTE').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('NORTE'));
});
$('#collapse-LITORAL').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('LITORAL'));
});
$('#collapse-METROPOLITANA').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('METROPOLITANA'));
});
$('#collapse-ENCOSTA-SERRA').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('ENCOSTA-SERRA'));
});
$('#collapse-SERRA').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('SERRA'));
});
$('#collapse-VALE-CAI').on('shown.bs.collapse', function() {
    map.selectObject(map.getObjectById('VALE-CAI'));
});

$('#mapa-ps div.amcharts-chart-div a').remove();

<?php
// Habilita a personalização de cabeçalho
add_theme_support('custom-header', array(
    'default-image'          => '',
	'width'                  => 830,
	'height'                 => 285,
	'flex-height'            => false,
	'flex-width'             => false,
	'uploads'                => true,
	'random-default'         => false,
	'header-text'            => false,
	'default-text-color'     => '',
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
));

// Registra os menus
register_nav_menus(
    array(
        'main' => 'Menu Principal',
        // 'footer' => 'Menu do Rodapé (Sitemap)',
    )
);

// Post Thumbnail
require_once('inc/post-thumbnails.php');

// Menu do Bootstrap
require_once('inc/wp_bootstrap_navwalker.php');

// Breadcrumb
require_once('inc/breadcrumb.php');

// Script Condicional
require_once('inc/script_conditional.php');

// Scripts & Styles
require_once('inc/assets.php');

// Widgets
require_once('inc/widgets.php');
require_once('inc/mapa_widget.php');
require_once('inc/resultados_widget.php');

// Tamanho do excerpt
require_once('inc/excerpt.php');

// Navlinks personalizados
// require_once('inc/navlinks.php');

// Adicionar PrettyPhoto automaticamente.
require_once('inc/prettyphoto_rel.php');

// Queries personalizadas em determinados templates.
require_once('inc/custom-queries.php');
